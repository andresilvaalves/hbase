package br.edu.up.bigdata;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HBaseAPI {

	@Inject
	private HBaseDAO dao;
	
	
	@RequestMapping("/scan/{tableName}")
    public String scan(@PathVariable(value="tableName") String tableName) {
		String retorno = "";
		try {
			retorno = dao.scan(tableName);
			
		} catch (Exception e) {
			retorno = e.getMessage();
		}
		
		return retorno;
	}
	
	@RequestMapping("/scanRowFilter/{tableName}/{rowPrefix}")
    public String scanFilter(@PathVariable(value="tableName") String tableName, @PathVariable(value="rowPrefix") String rowPrefix) {
		String retorno = "";
		try {
			retorno = dao.scanRowFilter(tableName, rowPrefix);
			
		} catch (Exception e) {
			e.printStackTrace();
			retorno = e.getMessage();
		}
		
		return retorno;
	}
	
	@RequestMapping("/scanValueFilter/{tableName}/{valueFilter}")
    public String scanValueFilter(@PathVariable(value="tableName") String tableName, @PathVariable(value="valueFilter") String valueFilter) {
		String retorno = "";
		try {
			retorno = dao.scanValueFilter(tableName, valueFilter);
			
		} catch (Exception e) {
			e.printStackTrace();
			retorno = e.getMessage();
		}
		
		return retorno;
	}
	
	@RequestMapping("/get/{tableName}/{rowId}")
    public String get(@PathVariable(value="tableName") String tableName, @PathVariable(value="rowId") String rowId) {
		String retorno = "";
		try {
			retorno = dao.get(tableName, rowId);
			
		} catch (Exception e) {
			e.printStackTrace();
			retorno = e.getMessage();
		}
		return retorno;
	}
	
	@RequestMapping(value = "/put/{tableName}/{rowId}/{family}/{qualifier}/{value}", method = RequestMethod.PUT )
    public String put(
    		@PathVariable(value="tableName") String tableName, 
    		@PathVariable(value="rowId") String rowId, 
    		@PathVariable(value="family") String family,
    		@PathVariable(value="qualifier") String qualifier,
    		@PathVariable(value="value") String value ) {
		String retorno = "";
		try {
			dao.put(tableName, rowId,family, qualifier, value);
			retorno = "Sucesso!";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}
	
	@RequestMapping(value = "/delete/{tableName}/{rowId}", method = RequestMethod.DELETE )
    public String delete(
    		@PathVariable(value="tableName") String tableName, 
    		@PathVariable(value="rowId") String rowId) {
		String retorno = "";
		try {
			dao.delete(tableName, rowId,"", "");
			retorno = "sucesso!";
		} catch (Exception e) {
			e.printStackTrace();
			retorno = e.getMessage();
		}
		return retorno;
	}
	
	@RequestMapping(value = "/delete/{tableName}/{rowId}/{family}/{qualifier}", method = RequestMethod.DELETE )
    public String delete(
    		@PathVariable(value="tableName") String tableName, 
    		@PathVariable(value="rowId") String rowId, 
    		@PathVariable(value="family") String family,
    		@PathVariable(value="qualifier") String qualifier) {
		String retorno = "";
		try {
			dao.delete(tableName, rowId,family, qualifier);
			retorno = "sucesso!";
		} catch (Exception e) {
			e.printStackTrace();
			retorno = e.getMessage();
		}
		return retorno;
	}
	
}
